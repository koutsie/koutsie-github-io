# Security Policy

## Reporting a Vulnerability
Please make a pull request with the details or contact me via IM's (https://koutsie.vlx.red/socials.html (telegram mainly)) asap.
